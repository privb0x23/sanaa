#! /usr/bin/env sh

. "./.env"

cargo fmt && cargo clippy && cargo build "${@}"

# eof
