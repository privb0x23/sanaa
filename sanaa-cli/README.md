# Sanaa CLI

Reads from stdin each line (new line separated) as a string and tries to parse a data type (using the `sanaa` library). If successful, writes a corresponding JSON data structure to stdout.

## Incantations

Domain:

```sh
ᐅ echo 'some.domain.com' | sanaa
```

## Example Output

Domain:

```json
{
  "domain": {
    "domain": "some.domain.com",
    "rdn": "domain.com",
    "tld": "com",
    "domain_norm": "some.domain.com",
    "rdn_norm": "domain.com",
    "tld_norm": "com",
    "domain_safe": "some.domain[.]com",
    "root_safe": "domain[.]com",
    "domain_norm_safe": "some.domain[.]com",
    "root_norm_safe": "domain[.]com",
    "tld_known": true,
    "tld_icann": true,
    "tld_private": false
  }
}
```

## Types

- Domain
  - `bar.foo.com`
- IPv4
  - `1.2.3.4`
- IPv6
  - `::1`
  - `::ffff:5.6.7.8`
- URI
  - `https://domain.tld/some/path?query#fragment`

## ToDo

- Documentation
- Tests

## Dependencies

- `sanaa`
- `serde_json`
- `anyhow`
- `tracing`
- `tracing-subscriber`

## Themes and Styles

- Tracing
- AnyHow and (in the future) ThisError
- Clippy: `cargo clippy`
- Rustfmt: `cargo fmt` / `rustfmt --check ./src/main.rs`

## Security and Safety

- No unsafe
  - Uses `#![forbid(unsafe_code)]` in `main.rs`
- `cargo outdated`
- `cargo audit`
- `cargo geiger`

## Rust Versions

- Stable only
- Tested on `1.59.0`

## Licence

[OpenBSD Licence](LICENCE).

Copyright (c) 2021-2022 privb0x23
