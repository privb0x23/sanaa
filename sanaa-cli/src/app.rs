// Copyright (c) 2021-2022 privb0x23
// OpenBSD Licence

// 0.1.1+02021-05-23

// ----------------------------------------------------------------------------

use ::std::io::{self, prelude::*};

use ::anyhow;

use ::tracing::{debug, trace, warn};

use ::serde_json;

use ::sanaa;

// ----------------------------------------------------------------------------

pub fn run() -> anyhow::Result<()> {
	if cfg!(debug_assertions) {
		trace!("running ...");
	}

	let stdin = io::stdin();
	for line in stdin.lock().lines() {
		match process_input(&line?) {
			Ok(_) => {}
			Err(e) => warn!("{}", e),
		};
	}

	Ok(())
}

// ----------------------------------------------------------------------------

fn process_input(input: &str) -> anyhow::Result<()> {
	if cfg!(debug_assertions) {
		trace!("process");
	}

	if input.is_empty() {
		debug!("empty input");
		return Ok(());
	}

	debug!("{}", input);

	match sanaa::parse_ipv4(input) {
		Ok(ipv4) => {
			let ip_ser = sanaa::IpAddrSer {
				ipaddr: &ipv4,
			};
			println!("{}", serde_json::to_string(&ip_ser)?);
			return Ok(());
		}
		Err(e) => debug!("{}", e),
	};
	match sanaa::parse_ipv6(input) {
		Ok(ipv6) => {
			let ip_ser = sanaa::IpAddrSer {
				ipaddr: &ipv6,
			};
			println!("{}", serde_json::to_string(&ip_ser)?);
			return Ok(());
		}
		Err(e) => debug!("{}", e),
	};
	match sanaa::parse_domain(input) {
		Ok(domain) => {
			let domain_ser = sanaa::DomainSer {
				domain: &domain,
			};
			println!("{}", serde_json::to_string(&domain_ser)?);
			return Ok(());
		}
		Err(e) => debug!("{}", e),
	}
	match sanaa::parse_uri(input) {
		Ok(uri) => {
			let uri_ser = sanaa::UriSer {
				uri: &uri,
			};
			println!("{}", serde_json::to_string(&uri_ser)?);
			return Ok(());
		}
		Err(e) => debug!("{}", e),
	};

	Err(anyhow::anyhow!("no matches"))
}

// ----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_ok_uris() {
		process_input("http://127.0.0.1/").unwrap();
		process_input("https://example.net/a/b.html").unwrap();
		process_input("https://example.net:8443/path?query&key=value#frag")
			.unwrap();
		process_input("ftp://example.com/foo").unwrap();
		process_input("ftp://rms:secret123@example.com").unwrap();
		process_input("blob:https://example.com/foo").unwrap();
		process_input("data:text/plain,Stuff").unwrap();
		process_input("ssh://example.com:2222").unwrap();
		process_input("mailto:rms@example.net").unwrap();
		process_input("unix:/run/foo.socket").unwrap();
		process_input("file:///tmp/foo").unwrap();
		process_input("foo://example.com").unwrap();
		process_input("foo:bar").unwrap();
		// process_input( "" ).unwrap();
	}

	#[test]
	fn test_ok_domains() {
		process_input("bar.foo.com").unwrap();
	}
}

// ----------------------------------------------------------------------------

// vim: set sw=4 sts=4 expandtab :
// eof app.rs
