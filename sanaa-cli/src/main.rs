// Copyright (c) 2021-2022 privb0x23
// OpenBSD Licence

// 0.1.1+02021-05-23

// ----------------------------------------------------------------------------

#![forbid(unsafe_code)]

// ----------------------------------------------------------------------------

use ::std;

use ::anyhow;

use ::tracing::{
	self,
	// debug,
	error,
	// event,
	// info,
	// instrument,
	// span,
	// trace,
	// warn,
	// Level,
};

use ::tracing_subscriber;

// ----------------------------------------------------------------------------

mod app;

// ----------------------------------------------------------------------------

pub fn main() -> anyhow::Result<()> {
	tracing::subscriber::set_global_default(
		tracing_subscriber::FmtSubscriber::builder()
			.with_env_filter(
				tracing_subscriber::EnvFilter::try_from_default_env()
					.or_else(|_| {
						tracing_subscriber::EnvFilter::try_new("info")
					})?,
			)
			.with_timer(tracing_subscriber::fmt::time::UtcTime::rfc_3339())
			.with_writer(std::io::stderr)
			.finish(),
	)?;

	let code = {
		if let Err(e) = app::run() {
			error!("error: {}", e,);
			1
		} else {
			0
		}
	};

	std::process::exit(code);
}

// ----------------------------------------------------------------------------

// mod tests;

// ----------------------------------------------------------------------------

// vim: set sw=4 sts=4 expandtab :
// eof main.rs
