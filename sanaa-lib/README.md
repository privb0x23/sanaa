# Sanaa Library

## Types

- Domain
  - `bar.foo.com`
- IPv4
  - `1.2.3.4`
- IPv6
  - `::1`
  - `::ffff:5.6.7.8`
- URI
  - `https://domain.tld/some/path?query#fragment`

## ToDo

- Documentation
- Add Socket type
  - `example.org:8443`
  - `10.9.8.7:53`
  - `[a:b:c::d]:636`
- Add CIDR type
  - `10.0.0.0/8`
- Add non-decimal IPv4
- Add non-hex IPv6
- Add e-mail type
- Add dns names to allowed Domain
- Add non-icann suffixes/TLDs
- Add map of scheme to default port
  - https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xml
- More tests

## Dependencies

- `addr`
- `idna`
- `once_cell`
- `regex`
- `serde`
- `url`
- `anyhow`
- `tracing`

## Themes and Styles

- Tracing
- AnyHow and (in the future) ThisError
- Clippy: `cargo clippy`
- Rustfmt: `cargo fmt` / `rustfmt --check ./src/lib.rs`

## Security and Safety

- No unsafe
  - Uses `#![forbid(unsafe_code)]` in `lib.rs`
- `cargo outdated`
- `cargo audit`
- `cargo geiger`

## Rust Versions

- Stable only
- Tested on `1.59.0`

## Licence

[OpenBSD Licence](LICENCE).

Copyright (c) 2021-2022 privb0x23
