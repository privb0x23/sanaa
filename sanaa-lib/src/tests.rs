// Copyright (c) 2021-2022 privb0x23
// OpenBSD Licence

// 0.1.1+02021-05-23

// ----------------------------------------------------------------------------

// https://raw.githubusercontent.com/publicsuffix/list/master/tests/test_psl.txt

#[cfg(test)]
mod tests {
	use crate::*;

	#[test]
	fn test_ok_domains_ok() {
		// valid

		let domain = parse_domain("bar.foo.com").unwrap();
		assert!(domain.is_suffix_known());
		assert!(domain.is_suffix_icann());
		assert!(!domain.is_suffix_private());
		assert_eq!("bar.foo.com", domain.as_str());
		assert_eq!("com", domain.suffix().unwrap());
		assert_eq!("bar.foo", domain.prefix().unwrap());
		assert_eq!("foo.com", domain.root().unwrap());

		let domain = parse_domain("domain.tld").unwrap();
		assert!(!domain.is_suffix_known());
		assert!(!domain.is_suffix_icann());
		assert!(!domain.is_suffix_private());

		let _domain = parse_domain("example.com.").unwrap();

		let domain = parse_domain("1.2.3.4.test.cyb").unwrap();
		assert!(!domain.is_suffix_known());
		assert!(!domain.is_suffix_icann());
		assert!(!domain.is_suffix_private());

		let domain = parse_domain("example.COM").unwrap();
		assert_eq!("com", domain.suffix().unwrap());
		assert_eq!("example", domain.prefix().unwrap());
		let domain = parse_domain("WwW.example.COM").unwrap();
		assert_eq!("com", domain.suffix().unwrap());
		assert_eq!("example.com", domain.root().unwrap());

		let domain = parse_domain("example.local").unwrap();
		assert_eq!("example.local", domain.root().unwrap());
		let domain = parse_domain("b.example.local").unwrap();
		assert_eq!("example.local", domain.root().unwrap());
		let domain = parse_domain("a.b.example.local").unwrap();
		assert_eq!("example.local", domain.root().unwrap());

		let domain = parse_domain("domain.biz").unwrap();
		assert_eq!("domain.biz", domain.root().unwrap());
		let domain = parse_domain("b.domain.biz").unwrap();
		assert_eq!("domain.biz", domain.root().unwrap());
		let domain = parse_domain("a.b.domain.biz").unwrap();
		assert_eq!("domain.biz", domain.root().unwrap());
		let domain = parse_domain("example.com").unwrap();
		assert_eq!("example.com", domain.root().unwrap());
		let domain = parse_domain("b.example.com").unwrap();
		assert_eq!("example.com", domain.root().unwrap());
		let domain = parse_domain("a.b.example.com").unwrap();
		assert_eq!("example.com", domain.root().unwrap());

		let domain = parse_domain("example.uk.com").unwrap();
		assert_eq!("example.uk.com", domain.root().unwrap());
		let domain = parse_domain("b.example.uk.com").unwrap();
		assert_eq!("example.uk.com", domain.root().unwrap());
		let domain = parse_domain("a.b.example.uk.com").unwrap();
		assert_eq!("example.uk.com", domain.root().unwrap());
		let domain = parse_domain("test.ac").unwrap();
		assert_eq!("test.ac", domain.root().unwrap());

		let domain = parse_domain("b.c.mm").unwrap();
		assert_eq!("b.c.mm", domain.root().unwrap());
		let domain = parse_domain("a.b.c.mm").unwrap();
		assert_eq!("b.c.mm", domain.root().unwrap());
		let domain = parse_domain("test.jp").unwrap();
		assert_eq!("test.jp", domain.root().unwrap());
		let domain = parse_domain("www.test.jp").unwrap();
		assert_eq!("test.jp", domain.root().unwrap());
		let domain = parse_domain("test.ac.jp").unwrap();
		assert_eq!("test.ac.jp", domain.root().unwrap());
		let domain = parse_domain("www.test.ac.jp").unwrap();
		assert_eq!("test.ac.jp", domain.root().unwrap());
		let domain = parse_domain("test.kyoto.jp").unwrap();
		assert_eq!("test.kyoto.jp", domain.root().unwrap());
		let domain = parse_domain("b.ide.kyoto.jp").unwrap();
		assert_eq!("b.ide.kyoto.jp", domain.root().unwrap());
		let domain = parse_domain("a.b.ide.kyoto.jp").unwrap();
		assert_eq!("b.ide.kyoto.jp", domain.root().unwrap());
		let domain = parse_domain("b.c.kobe.jp").unwrap();
		assert_eq!("b.c.kobe.jp", domain.root().unwrap());
		let domain = parse_domain("a.b.c.kobe.jp").unwrap();
		assert_eq!("b.c.kobe.jp", domain.root().unwrap());
		let domain = parse_domain("city.kobe.jp").unwrap();
		assert_eq!("city.kobe.jp", domain.root().unwrap());
		let domain = parse_domain("www.city.kobe.jp").unwrap();
		assert_eq!("city.kobe.jp", domain.root().unwrap());
		let domain = parse_domain("b.test.ck").unwrap();
		assert_eq!("b.test.ck", domain.root().unwrap());
		let domain = parse_domain("a.b.test.ck").unwrap();
		assert_eq!("b.test.ck", domain.root().unwrap());
		let domain = parse_domain("www.ck").unwrap();
		assert_eq!("www.ck", domain.root().unwrap());
		let domain = parse_domain("www.www.ck").unwrap();
		assert_eq!("www.ck", domain.root().unwrap());

		let domain = parse_domain("test.us").unwrap();
		assert_eq!("test.us", domain.root().unwrap());
		let domain = parse_domain("www.test.us").unwrap();
		assert_eq!("test.us", domain.root().unwrap());
		let domain = parse_domain("test.ak.us").unwrap();
		assert_eq!("test.ak.us", domain.root().unwrap());
		let domain = parse_domain("www.test.ak.us").unwrap();
		assert_eq!("test.ak.us", domain.root().unwrap());
		let domain = parse_domain("test.k12.ak.us").unwrap();
		assert_eq!("test.k12.ak.us", domain.root().unwrap());
		let domain = parse_domain("www.test.k12.ak.us").unwrap();
		assert_eq!("test.k12.ak.us", domain.root().unwrap());

		let domain = parse_domain("食狮.com.cn").unwrap();
		assert_eq!("食狮.com.cn", domain.root().unwrap());
		let domain = parse_domain("食狮.公司.cn").unwrap();
		assert_eq!("食狮.公司.cn", domain.root().unwrap());
		let domain = parse_domain("www.食狮.公司.cn").unwrap();
		assert_eq!("食狮.公司.cn", domain.root().unwrap());
		let domain = parse_domain("shishi.公司.cn").unwrap();
		assert_eq!("shishi.公司.cn", domain.root().unwrap());
		let domain = parse_domain("食狮.中国").unwrap();
		assert_eq!("食狮.中国", domain.root().unwrap());
		let domain = parse_domain("www.食狮.中国").unwrap();
		assert_eq!("食狮.中国", domain.root().unwrap());
		let domain = parse_domain("shishi.中国").unwrap();
		assert_eq!("shishi.中国", domain.root().unwrap());

		let domain = parse_domain("xn--85x722f.com.cn").unwrap();
		assert_eq!("xn--85x722f.com.cn", domain.root().unwrap());
		let domain = parse_domain("xn--85x722f.xn--55qx5d.cn").unwrap();
		assert_eq!("xn--85x722f.xn--55qx5d.cn", domain.root().unwrap());
		let domain = parse_domain("www.xn--85x722f.xn--55qx5d.cn").unwrap();
		assert_eq!("xn--85x722f.xn--55qx5d.cn", domain.root().unwrap());
		let domain = parse_domain("shishi.xn--55qx5d.cn").unwrap();
		assert_eq!("shishi.xn--55qx5d.cn", domain.root().unwrap());
		let domain = parse_domain("xn--85x722f.xn--fiqs8s").unwrap();
		assert_eq!("xn--85x722f.xn--fiqs8s", domain.root().unwrap());
		let domain = parse_domain("www.xn--85x722f.xn--fiqs8s").unwrap();
		assert_eq!("xn--85x722f.xn--fiqs8s", domain.root().unwrap());
		let domain = parse_domain("shishi.xn--fiqs8s").unwrap();
		assert_eq!("shishi.xn--fiqs8s", domain.root().unwrap());

		let _domain = parse_domain(
			"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcde.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.com."
		).unwrap();
	}

	#[test]
	fn test_ok_domains_fail() {
		// invalid

		let domain = parse_domain("1.2.3.4");
		assert!(domain.is_err());

		let domain = parse_domain("_tcp.example.com.");
		assert!(domain.is_err());

		let domain = parse_domain("");
		assert!(domain.is_err());
		let domain = parse_domain("COM");
		assert!(domain.is_err());
		let domain = parse_domain(".com");
		assert!(domain.is_err());
		let domain = parse_domain(".example");
		assert!(domain.is_err());
		let domain = parse_domain(".example.com");
		assert!(domain.is_err());
		let domain = parse_domain(".example.example");
		assert!(domain.is_err());
		let domain = parse_domain("example");
		assert!(domain.is_err());
		let domain = parse_domain("local");
		assert!(domain.is_err());
		let domain = parse_domain("biz");
		assert!(domain.is_err());
		let domain = parse_domain("uk.com");
		assert!(domain.is_err());
		let domain = parse_domain("mm");
		assert!(domain.is_err());
		let domain = parse_domain("c.mm");
		assert!(domain.is_err());
		let domain = parse_domain("jp");
		assert!(domain.is_err());
		let domain = parse_domain("ac.jp");
		assert!(domain.is_err());
		let domain = parse_domain("kyoto.jp");
		assert!(domain.is_err());
		let domain = parse_domain("ide.kyoto.jp");
		assert!(domain.is_err());
		let domain = parse_domain("c.kobe.jp");
		assert!(domain.is_err());
		let domain = parse_domain("ck");
		assert!(domain.is_err());
		let domain = parse_domain("test.ck");
		assert!(domain.is_err());
		let domain = parse_domain("us");
		assert!(domain.is_err());
		let domain = parse_domain("ak.us");
		assert!(domain.is_err());
		let domain = parse_domain("k12.ak.us");
		assert!(domain.is_err());
		let domain = parse_domain("公司.cn");
		assert!(domain.is_err());
		let domain = parse_domain("中国");
		assert!(domain.is_err());
		let domain = parse_domain("xn--55qx5d.cn");
		assert!(domain.is_err());
		let domain = parse_domain("xn--fiqs8s");
		assert!(domain.is_err());

		let domain = parse_domain(
			"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcde0.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.com."
		);
		assert!(domain.is_err());
	}

	#[test]
	fn test_ok_make_safe() {
		// valid

		let string = "foo.com";
		let result = make_safe(string).unwrap();
		assert_eq!("foo[.]com", result);
		let string = "bar.foo.com";
		let result = make_safe(string).unwrap();
		assert_eq!("bar.foo[.]com", result);
		let string = "1.2.3.4";
		let result = make_safe(string).unwrap();
		assert_eq!("1.2.3[.]4", result);
		let string = "::1";
		let result = make_safe(string).unwrap();
		assert_eq!("[::]1", result);
		let string = "a:b::c:d";
		let result = make_safe(string).unwrap();
		assert_eq!("a:b::c[:]d", result);
		let string = "www.食狮.中国";
		let result = make_safe(string).unwrap();
		assert_eq!("www.食狮[.]中国", result);
		let string = "foo[.]com";
		let result = make_safe(string).unwrap();
		assert_eq!("foo[.]com", result);
		let string = "foo.c-m";
		let result = make_safe(string).unwrap();
		assert_eq!("foo[.]c-m", result);
		let string = "foo[.com";
		let result = make_safe(string).unwrap();
		assert_eq!("foo[.com", result);
		let string = "foo.]com";
		let result = make_safe(string).unwrap();
		assert_eq!("foo.]com", result);
		let string = "foo";
		let result = make_safe(string).unwrap();
		assert_eq!("foo", result);
	}
}

// ----------------------------------------------------------------------------

// vim: set sw=4 sts=4 expandtab :
// eof tests.rs
