// Copyright (c) 2021-2022 privb0x23
// OpenBSD Licence

// 0.1.1+02021-05-23

// ----------------------------------------------------------------------------

use ::std::{
	clone::Clone, fmt, fmt::Debug, fmt::Display, fmt::Formatter,
	option::Option, result::Result, string::String,
};

use ::anyhow;

use ::tracing::trace;

use ::addr::{self, parser::DomainName};
use ::idna;

use ::serde::{self, ser::SerializeStruct};

// ----------------------------------------------------------------------------

//
#[derive(Clone, Debug)]
pub struct Domain {
	domain: String,
	suffix_start: usize,
	prefix_end: usize,
	root_start: usize,
	is_suffix_known: bool,
	is_suffix_icann: bool,
	is_suffix_private: bool,
	normalised: Option<String>,
}

//
#[derive(Debug, serde::Serialize)]
pub struct DomainSer<'a> {
	pub domain: &'a Domain,
}

// ----------------------------------------------------------------------------

//
pub fn parse_domain(domain: &str) -> anyhow::Result<Domain> {
	if cfg!(debug_assertions) {
		trace!("parse_domain");
	}

	let domain = domain.to_ascii_lowercase();
	match addr::psl::List.parse_domain_name(&domain) {
		Ok(domain) => {
			if cfg!(debug_assertions) {
				trace!("tld: {}", domain.suffix());
				trace!("known suffix? {}", domain.has_known_suffix());
			}
			match domain.root() {
				Some(root) => {
					if cfg!(debug_assertions) {
						trace!("rdn: {}", root);
					}
				}
				None => return Err(anyhow::anyhow!("invalid root domain")),
			};

			Ok(Domain::try_new(&domain)?)
		}
		Err(e) => Err(anyhow::anyhow!("invalid domain: {}", e)),
	}
}

// ----------------------------------------------------------------------------

//
impl Domain {
	//
	pub fn try_new(domain_name: &addr::domain::Name) -> anyhow::Result<Self> {
		if cfg!(debug_assertions) {
			trace!("try_new");
		}

		let domain_str = domain_name.as_str();

		// strip off any final '.'
		let domain_str = if let Some(stripped) = domain_str.strip_suffix('.') {
			stripped
		} else {
			domain_str
		};

		// convert to ascii
		let domain_ascii = idna::domain_to_ascii(domain_str)?;

		// max = 63+1+63+1+63+1+62 = 253
		let domain_len = domain_ascii.len();
		if 0 == domain_len
			|| 253 < domain_len
			|| domain_ascii
				.split('.')
				.any(|label| label.is_empty() || 63 < label.len())
		{
			return Err(anyhow::anyhow!("invalid domain length"));
		}

		// get the parsed suffix
		let suffix = domain_name.suffix();

		// strip off any final '.'
		let suffix = if let Some(stripped) = suffix.strip_suffix('.') {
			stripped
		} else {
			suffix
		};
		// ensure lowercase ascii
		let suffix_lowercase = suffix.to_ascii_lowercase();

		// allocate new string
		let domain = String::from(domain_str);
		// this might need different handling for non-punycode IDN
		// let domain = domain_str.to_ascii_lowercase();

		let suffix_start = match domain_str.rfind(&suffix_lowercase) {
			Some(offset) => offset,
			None => return Err(anyhow::anyhow!("invalid domain suffix")),
		};

		// calculate the end of the prefix
		let prefix_end = if suffix_start > 0 {
			let single_char = match domain
				.get((suffix_start - 1)..suffix_start)
			{
				Some(slice) => slice,
				None => {
					return Err(anyhow::anyhow!("invalid domain substring"))
				}
			};
			if "." == single_char {
				suffix_start - 1
			} else {
				0
			}
		} else {
			0
		};

		// get the prefix sub-string
		let prefix = match domain.get(..prefix_end) {
			Some(slice) => slice,
			None => return Err(anyhow::anyhow!("invalid domain prefix")),
		};

		// calculate the start of the rdn
		let root_start = prefix.rfind('.').map(|x| x + 1).unwrap_or_default();

		let normalised = if domain_ascii == domain {
			None
		} else {
			Some(domain_ascii)
		};

		// TODO: add other TLDs (opennic, tor)

		Ok(Self {
			domain,
			suffix_start,
			prefix_end,
			root_start,
			is_suffix_known: domain_name.has_known_suffix(),
			is_suffix_icann: domain_name.is_icann(),
			is_suffix_private: domain_name.is_private(),
			normalised,
		})
	}

	//
	pub fn as_str(&self) -> &str {
		&self.domain
	}

	//
	pub fn suffix(&self) -> Option<&str> {
		self.domain.get(self.suffix_start..)
	}

	//
	pub fn prefix(&self) -> Option<&str> {
		self.domain.get(..self.prefix_end)
	}

	//
	pub fn root(&self) -> Option<&str> {
		self.domain.get(self.root_start..)
	}

	//
	pub fn is_suffix_known(&self) -> bool {
		self.is_suffix_known
	}

	//
	pub fn is_suffix_icann(&self) -> bool {
		self.is_suffix_icann
	}

	//
	pub fn is_suffix_private(&self) -> bool {
		self.is_suffix_private
	}
}

//
impl Display for Domain {
	//
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.domain)
	}
}

//
impl serde::ser::Serialize for Domain {
	//
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::ser::Serializer,
	{
		let suffix = match self.suffix() {
			Some(suffix) => suffix,
			None => return Err(serde::ser::Error::custom("invalid data")),
		};

		let root = match self.root() {
			Some(root) => root,
			None => return Err(serde::ser::Error::custom("invalid data")),
		};

		let norm = match &self.normalised {
			Some(norm) => norm,
			None => &self.domain,
		};

		let root_norm = match idna::domain_to_ascii(root) {
			Ok(root) => root,
			Err(e) => {
				return Err(serde::ser::Error::custom(format!(
					"invalid data: {}",
					e
				)))
			}
		};

		let suffix_norm = match idna::domain_to_ascii(suffix) {
			Ok(suffix) => suffix,
			Err(e) => {
				return Err(serde::ser::Error::custom(format!(
					"invalid data: {}",
					e
				)))
			}
		};

		let domain_safe = match crate::make_safe(&self.domain) {
			Ok(safe) => safe,
			Err(e) => {
				return Err(serde::ser::Error::custom(format!(
					"invalid data: {}",
					e
				)))
			}
		};

		let root_safe = match crate::make_safe(root) {
			Ok(safe) => safe,
			Err(e) => {
				return Err(serde::ser::Error::custom(format!(
					"invalid data: {}",
					e
				)))
			}
		};

		let domain_norm_safe = match crate::make_safe(norm) {
			Ok(safe) => safe,
			Err(e) => {
				return Err(serde::ser::Error::custom(format!(
					"invalid data: {}",
					e
				)))
			}
		};

		let root_norm_safe = match crate::make_safe(&root_norm) {
			Ok(safe) => safe,
			Err(e) => {
				return Err(serde::ser::Error::custom(format!(
					"invalid data: {}",
					e
				)))
			}
		};

		let mut state = serializer.serialize_struct("domain", 8)?;
		state.serialize_field("domain", &self.domain)?;
		state.serialize_field("rdn", &root)?;
		state.serialize_field("tld", &suffix)?;
		state.serialize_field("domain_norm", &norm)?;
		state.serialize_field("rdn_norm", &root_norm)?;
		state.serialize_field("tld_norm", &suffix_norm)?;
		state.serialize_field("domain_safe", &domain_safe)?;
		state.serialize_field("root_safe", &root_safe)?;
		state.serialize_field("domain_norm_safe", &domain_norm_safe)?;
		state.serialize_field("root_norm_safe", &root_norm_safe)?;
		state.serialize_field("tld_known", &self.is_suffix_known())?;
		state.serialize_field("tld_icann", &self.is_suffix_icann())?;
		state.serialize_field("tld_private", &self.is_suffix_private())?;
		state.end()
	}
}

// ----------------------------------------------------------------------------

// tests

// ----------------------------------------------------------------------------

// vim: set sw=4 sts=4 expandtab :
// eof domain.rs
