// Copyright (c) 2021-2022 privb0x23
// OpenBSD Licence

// 0.1.1+02021-05-23

// ----------------------------------------------------------------------------

use ::std::{
	clone::Clone, fmt, fmt::Debug, fmt::Display, fmt::Formatter, net,
	result::Result, string::String,
};

use ::anyhow;

use ::tracing::trace;

use ::serde::{self, ser::SerializeStruct};

// ----------------------------------------------------------------------------

//
#[derive(Clone, Debug)]
pub struct IpAddr {
	inner_ipaddr: net::IpAddr,
}

//
#[derive(Debug, serde::Serialize)]
pub struct IpAddrSer<'a> {
	pub ipaddr: &'a IpAddr,
}

// ----------------------------------------------------------------------------

//
pub fn parse_ipv4(ip: &str) -> anyhow::Result<IpAddr> {
	if cfg!(debug_assertions) {
		trace!("parse_ipv4");
	}

	match ip.parse::<net::IpAddr>() {
		Ok(ip) => {
			match ip {
				net::IpAddr::V4(_ipv4) => {
					if cfg!(debug_assertions) {
						trace!("ipv4");
						// trace!("global? {}", ipv4.is_global());
					}
				}
				_ => return Err(anyhow::anyhow!("invalid ipv4 address")),
			};

			Ok(IpAddr::new(ip))
		}
		Err(e) => Err(anyhow::anyhow!("invalid ip address: {}", e)),
	}
}

//
pub fn parse_ipv6(ip: &str) -> anyhow::Result<IpAddr> {
	if cfg!(debug_assertions) {
		trace!("parse_ipv6");
	}

	match ip.parse::<net::IpAddr>() {
		Ok(ip) => {
			match ip {
				net::IpAddr::V6(_ipv6) => {
					if cfg!(debug_assertions) {
						trace!("ipv6");
						// trace!("global? {}", ipv6.is_global());
					}
				}
				_ => return Err(anyhow::anyhow!("invalid ipv6 address")),
			};

			Ok(IpAddr::new(ip))
		}
		Err(e) => Err(anyhow::anyhow!("invalid ip address: {}", e)),
	}
}

// ----------------------------------------------------------------------------

//
impl IpAddr {
	//
	pub fn new(inner_ipaddr: net::IpAddr) -> Self {
		Self {
			inner_ipaddr,
		}
	}

	//
	pub fn display(&self) -> String {
		format!("{}", &self.inner_ipaddr)
	}

	//
	pub fn ipaddr(&self) -> &net::IpAddr {
		&self.inner_ipaddr
	}
}

//
impl Display for IpAddr {
	//
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}", &self.inner_ipaddr)
	}
}

//
impl serde::ser::Serialize for IpAddr {
	//
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::ser::Serializer,
	{
		let ip_type = match self.inner_ipaddr {
			net::IpAddr::V4(_ipv4) => "v4",
			net::IpAddr::V6(_ipv6) => "v6",
		};

		// routing: loopback, private, global, unspecified, documentation
		let route = if self.inner_ipaddr.is_loopback() {
			"loopback"
		} else {
			// TODO: add more variants
			// ip.is_private()
			// ip.is_global()
			// ip.is_documentation()
			"non-loopback"
		};

		let mut state = serializer.serialize_struct("ipaddr", 4)?;
		state.serialize_field("ipaddr", &format!("{}", self.inner_ipaddr))?;
		state.serialize_field("type", &ip_type)?;
		state.serialize_field("route", &route)?;
		// state.serialize_field("bogon", &bogon)?;
		state
			.serialize_field("multicast", &self.inner_ipaddr.is_multicast())?;
		state.end()
	}
}

// ----------------------------------------------------------------------------

// tests

// ----------------------------------------------------------------------------

// vim: set sw=4 sts=4 expandtab :
// eof ipaddr.rs
