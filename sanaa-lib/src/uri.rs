// Copyright (c) 2021-2022 privb0x23
// OpenBSD Licence

// 0.1.1+02021-05-23

// ----------------------------------------------------------------------------

use ::std::{
	clone::Clone, fmt, fmt::Debug, fmt::Display, fmt::Formatter, net,
	result::Result, string::String,
};

use ::anyhow;

use ::tracing::trace;

use ::url;

use ::serde::{self, ser::SerializeStruct};

use crate::domain;
use crate::ipaddr;

// ----------------------------------------------------------------------------

//
#[derive(Clone, Debug)]
pub enum Host {
	Domain(domain::Domain),
	Ipv4(ipaddr::IpAddr),
	Ipv6(ipaddr::IpAddr),
	None,
}

// ----------------------------------------------------------------------------

//
#[derive(Clone, Debug)]
pub struct Uri {
	inner_uri: url::Url,
	host: Host,
}

// #[derive(Debug, serde::Serialize)]
// struct UriSer<'a> {
// 	uri: &'a str,
// 	host: &'a Host,
// }

//
#[derive(Debug, serde::Serialize)]
pub struct UriSer<'a> {
	pub uri: &'a Uri,
}

// ----------------------------------------------------------------------------

//
pub fn parse_uri(uri: &str) -> anyhow::Result<Uri> {
	if cfg!(debug_assertions) {
		trace!("parse_uri");
	}

	match url::Url::parse(uri) {
		Ok(uri) => {
			let host = uri.host();
			match host {
				Some(url::Host::Domain(host)) => {
					let host = String::from(host);
					let domain = domain::parse_domain(&host);
					match domain {
						Ok(domain) => {
							Ok(Uri::new(uri.clone(), Host::Domain(domain)))
						}
						Err(e) => {
							Err(anyhow::anyhow!("invalid uri domain: {}", e))
						}
					}
				}
				Some(url::Host::Ipv4(ip)) => {
					if cfg!(debug_assertions) {
						trace!("ipv4")
					}
					Ok(Uri::new(
						uri.clone(),
						Host::Ipv4(ipaddr::IpAddr::new(net::IpAddr::from(ip))),
					))
				}
				Some(url::Host::Ipv6(ip)) => {
					if cfg!(debug_assertions) {
						trace!("ipv6")
					}
					Ok(Uri::new(
						uri.clone(),
						Host::Ipv6(ipaddr::IpAddr::new(net::IpAddr::from(ip))),
					))
				}
				None => {
					// Err(anyhow::anyhow!("invalid uri host"))
					Ok(Uri::new(uri.clone(), Host::None))
				}
			}
		}
		Err(e) => Err(anyhow::anyhow!("invalid uri: {}", e)),
	}
}

// ----------------------------------------------------------------------------

//
impl Display for Host {
	//
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Host::Domain(domain) => {
				write!(f, "{}", domain.as_str())
			}
			Host::Ipv4(ip) => Display::fmt(&ip, f),
			Host::Ipv6(ip) => Display::fmt(&ip, f),
			Host::None => write!(f, "None"),
		}
	}
}

//
impl serde::ser::Serialize for Host {
	//
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::ser::Serializer,
	{
		match self {
			Host::Domain(domain) => {
				let domain_ser = domain::DomainSer {
					domain,
				};
				serializer.serialize_newtype_struct("domain", &domain_ser)
			}
			Host::Ipv4(ip) => {
				let ip_ser = ipaddr::IpAddrSer {
					ipaddr: ip,
				};
				serializer.serialize_newtype_struct("ipaddr", &ip_ser)
			}
			Host::Ipv6(ip) => {
				let ip_ser = ipaddr::IpAddrSer {
					ipaddr: ip,
				};
				serializer.serialize_newtype_struct("ipaddr", &ip_ser)
			}
			Host::None => serializer.serialize_none(),
		}
	}
}

// ----------------------------------------------------------------------------

//
impl Uri {
	//
	pub fn new(inner_uri: url::Url, host: Host) -> Self {
		Self {
			inner_uri,
			host,
		}
	}

	//
	pub fn as_str(&self) -> &str {
		self.inner_uri.as_str()
	}

	//
	pub fn uri(&self) -> &url::Url {
		&self.inner_uri
	}

	//
	pub fn host(&self) -> &Host {
		&self.host
	}
}

//
impl Display for Uri {
	//
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.as_str())
	}
}

//
impl serde::ser::Serialize for Uri {
	//
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::ser::Serializer,
	{
		let mut state = serializer.serialize_struct("uri", 2)?;
		state.serialize_field("uri", &self.inner_uri.as_str())?;
		state.serialize_field("host", &self.host)?;
		state.end()
	}
}

// ----------------------------------------------------------------------------

// tests

// ----------------------------------------------------------------------------

// vim: set sw=4 sts=4 expandtab :
// eof uri.rs
