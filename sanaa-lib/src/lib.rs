// Copyright (c) 2021-2022 privb0x23
// OpenBSD Licence

// 0.1.1+02021-05-23

// ----------------------------------------------------------------------------

#![forbid(unsafe_code)]

// ----------------------------------------------------------------------------

use ::std::borrow::Cow;

use ::regex;

use ::once_cell::sync::OnceCell;

// ----------------------------------------------------------------------------

mod domain;
mod ipaddr;
mod uri;

pub use domain::*;
pub use ipaddr::*;
pub use uri::*;

// ----------------------------------------------------------------------------

static RE_SAFE: OnceCell<regex::Regex> = OnceCell::new();

// ----------------------------------------------------------------------------

//
pub fn make_safe(input: &str) -> anyhow::Result<Cow<'_, str>> {
	let re_safe = RE_SAFE.get_or_try_init(|| {
		regex::Regex::new(
			r#"(?P<pre>^|[^\[])(?P<sep>\.|[:]{1,2})(?P<last>[^\.:\]]+)$"#,
		)
	})?;

	let output = re_safe.replace_all(input, "${pre}[${sep}]${last}");

	Ok(output)
}

// ----------------------------------------------------------------------------

mod tests;

// ----------------------------------------------------------------------------

// vim: set sw=4 sts=4 expandtab :
// eof lib.rs
