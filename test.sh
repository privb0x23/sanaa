#! /usr/bin/env sh

. "./.env"

export RUST_LOG='trace'

cargo test --target=x86_64-unknown-linux-musl "${@}"

# eof
