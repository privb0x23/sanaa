#! /usr/bin/env sh

. "./.env"

export RUST_LOG='trace'

cargo run -- "${@}"

# eof
