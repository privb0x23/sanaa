# Sanaa

Code name: SANAA

Public name: TBD

## About

A simple command line tool and Rust library to parse strings into some network related data types.

## CLI

Reads strings from stdin and outputs the relevant JSON when parsable. See [cli README](sanaa-cli/README.md) for more detail.

## Library

Parse data types and some utility functions. See [library README](sanaa-lib/README.md) for more detail.

## Licence

[OpenBSD Licence](LICENCE).

Copyright (c) 2021-2022 privb0x23
